CXX = g++

CXXFLAGS = -std=c++11 -Wall -lX11 -lXi -lXmu -lglut -lGL -lGLU -lm

SRC_DIR = src
INCLUDE_DIR = include

INCLUDE = -I $(INCLUDE_DIR) -I $(INCLUDE_DIR)/buildings -I $(INCLUDE_DIR)/util -I /usr/include/
LIBRARIES = -L /usr/lib/

PROGRAM = CivilianHumvees

.PHONY: all
all: $(PROGRAM)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	rm -rf *~ $(SRC_DIR)/*.o

.PHONY: clean-all
clean-all: clean
	rm -rf $(PROGRAM)

$(PROGRAM):
	$(CXX) $(CXXFLAGS) -o $@ $(LIBRARIES) $(INCLUDE) $(SRC_DIR)/*.cpp $(SRC_DIR)/buildings/*.cpp

.PHONY: test
test: $(PROGRAM)
	./$(PROGRAM)
