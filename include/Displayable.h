#ifndef DISPLAYABLE_H
#define DISPLAYABLE_H

class Displayable {
  public:
    virtual ~Displayable() {}

    virtual void display() = 0;
};

#endif /* DISPLAYABLE_H */
