#ifndef CAR_H
#define CAR_H

#include "WorldObject.h"

#define CAR_WIDTH 5.0
#define CAR_HEIGHT 12.0
#define CAR_DEPTH 6.0

class Car : public WorldObject {
  public:
    Car(Coord pos, Coord orientation, Coord size): WorldObject(pos, orientation,
                size), renderAccessories(false) {}

    virtual ~Car() {}

    virtual void display() override;

    virtual void animate(double secsElapsed) override;

    virtual void displayBody(double x1, double y1, double z1, double x2, double y2,
                             double z2);

    virtual void displayTop(double x1, double y1, double z1, double x2, double y2,
                            double z2);

    virtual void displayRocket(double x1, double y1, double z1, double x2,
                               double y2, double z2);

    virtual void setRenderAccessories(bool b)
    {
        this->renderAccessories = b;
    }

    virtual bool getRenderAccessories() const
    {
        return this->renderAccessories;
    }
  private:
    double animRotation = rand() % 360;

    bool renderAccessories;
};

#endif /* CAR_H */
