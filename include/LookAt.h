#ifndef LOOKAT_H
#define LOOKAT_H

#include "Coord.h"

class LookAt {
  public:
    virtual ~LookAt() {};

    virtual Coord getEye() = 0;

    virtual Coord getCenter() = 0;

    virtual Coord &getEyeOffset() = 0;

    virtual Coord &getCenterOffset() = 0;

    virtual Coord &getRotation() = 0;
};

#endif /* LOOKAT_H */
