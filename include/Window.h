#ifndef WINDOW_H
#define WINDOW_H

#include <memory>
#include "Viewport.h"

using std::shared_ptr;
using std::make_shared;

class Window {
  public:
    Window(const int id, const int width, const int height): id(id), width(width),
        height(height), viewport(make_shared<Viewport>()) {}

    virtual ~Window() {}

    virtual int getID() const
    {
        return this->id;
    }

    virtual int getWidth() const
    {
        return this->width;
    }

    virtual int getHeight() const
    {
        return this->height;
    }

    virtual shared_ptr<Viewport> getViewport() const
    {
        return this->viewport;
    }

    virtual shared_ptr<Viewport> getViewport()
    {
        return this->viewport;
    }

    virtual void setSize(const int width, const int height)
    {
        this->width = width;
        this->height = height;
    }
  private:
    int id, width, height;

    shared_ptr<Viewport> viewport;
};

#endif /* WINDOW_H */
