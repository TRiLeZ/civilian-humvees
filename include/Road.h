#ifndef ROAD_H
#define ROAD_H

#include "WorldObject.h"

#define ROAD_LINE_COUNT 32
#define ROAD_LINE_SIZE 0.5
#define ROAD_SIZE 10.0
#define ROAD_DEPTH 1.0
#define ROAD_DEPTH_OFFSET 0.1

class Road : public WorldObject {
  public:
    Road(Coord pos, Coord orientation, Coord size): WorldObject(pos, orientation,
                size) {}

    virtual ~Road() {}

    virtual void display() override;
};

#endif /* ROAD_H */
