#ifndef COORD_H
#define COORD_H

class Coord {
  public:
    Coord(): x(0.0), y(0.0), z(0.0) {}

    Coord(const double x, const double y, const double z): x(x), y(y), z(z) {}

    Coord(const Coord &c): x(c.getX()), y(c.getY()), z(c.getZ()) {}

    virtual ~Coord() {}

    virtual double getX() const
    {
        return this->x;
    }

    virtual double getY() const
    {
        return this->y;
    }

    virtual double getZ() const
    {
        return this->z;
    }

    virtual Coord &setX(const double x)
    {
        this->x = x;

        return *this;
    }

    virtual Coord &setY(const double y)
    {
        this->y = y;

        return *this;
    }

    virtual Coord &setZ(const double z)
    {
        this->z = z;

        return *this;
    }

    virtual Coord &setLocation(const double x, const double y, const double z)
    {
        setX(x);
        setY(y);
        setZ(z);

        return *this;
    }

    virtual Coord &setLocation(const Coord &c)
    {
        setX(c.getX());
        setY(c.getY());
        setZ(c.getZ());

        return *this;
    }

    virtual Coord &translate(const double dx, const double dy, const double dz)
    {
        setLocation(getX() + dx, getY() + dy, getZ() + dz);

        return *this;
    }

    virtual Coord &translate(const Coord &c)
    {
        return translate(c.getX(), c.getY(), c.getZ());
    }

    virtual Coord &operator=(const Coord &c)
    {
        setLocation(c);

        return *this;
    }

    virtual bool operator==(const Coord &c) const;

    virtual Coord operator*(double d) const;

    virtual Coord operator+(const Coord &c) const;

    virtual Coord operator-(const Coord &c) const;

    virtual Coord operator-() const;
  private:
    double x, y, z;
};

#endif /* COORD_H */
