#ifndef FOLLOWING_VIEW_H
#define FOLLOWING_VIEW_H

#include <memory>
#include "LookAt.h"
#include "util/Math.h"
#include "WorldObject.h"

using std::shared_ptr;

class FollowingView : public LookAt {
  public:
    FollowingView(shared_ptr<WorldObject> obj): obj(obj),
        eyeOffset(Coord()), centerOffset(Coord()), rotation(Coord()) {}

    virtual ~FollowingView() {}

    virtual Coord getEye() override
    {
        Coord eye = this->obj->getBoundingBox().getCenter();

        Coord offset = getEyeOffset();

        // Rotate offset as per the view direction
        offset = rotate(offset, Coord(1.0, 0.0, 0.0),
                        this->obj->getOrientation().getX());
        offset = rotate(offset, Coord(0.0, 1.0, 0.0),
                        this->obj->getOrientation().getY());
        offset = rotate(offset, Coord(0.0, 0.0, 1.0),
                        this->obj->getOrientation().getZ());

        return eye + offset;
    }

    virtual Coord getCenter() override
    {
        Box boundingBox = this->obj->getBoundingBox();

        Coord objCenter = boundingBox.getCenter();

        Coord lookCenter = objCenter;
        lookCenter = lookCenter.translate(0.0, 0.0,
                                          this->obj->getSize().getZ() / 2.0).translate(getCenterOffset());

        Coord rotated = lookCenter - objCenter;

        // Rotate view as per the object's orientation
        rotated = rotate(rotated, Coord(1.0, 0.0, 0.0),
                         this->obj->getOrientation().getX());
        rotated = rotate(rotated, Coord(0.0, 1.0, 0.0),
                         this->obj->getOrientation().getY());
        rotated = rotate(rotated, Coord(0.0, 0.0, 1.0),
                         this->obj->getOrientation().getZ());

        // Rotate view as per the view direction
        rotated = rotate(rotated, Coord(1.0, 0.0, 0.0), getRotation().getX());
        rotated = rotate(rotated, Coord(0.0, 1.0, 0.0), getRotation().getY());
        rotated = rotate(rotated, Coord(0.0, 0.0, 1.0), getRotation().getZ());

        return rotated + objCenter;
    }

    virtual Coord &getEyeOffset() override
    {
        return this->eyeOffset;
    }

    virtual Coord &getCenterOffset() override
    {
        return this->centerOffset;
    }

    virtual Coord &getRotation() override
    {
        return this->rotation;
    }
  private:
    shared_ptr<WorldObject> obj;

    Coord eyeOffset;
    Coord centerOffset;
    Coord rotation;
};

#endif /* FOLLOWING_VIEW_H */
