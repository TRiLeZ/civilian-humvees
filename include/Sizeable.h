#ifndef SIZEABLE_H
#define SIZEABLE_H

#include "Coord.h"

class Sizeable {
  public:
    virtual ~Sizeable() {}

    virtual Coord &getSize() = 0;
};

#endif /* SIZEABLE_H */
