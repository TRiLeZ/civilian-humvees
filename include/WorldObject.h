#ifndef WORLDOBJECT_H
#define WORLDOBJECT_H

#include "Box.h"
#include "Displayable.h"
#include "Positionable.h"
#include "Rotatable.h"
#include "Sizeable.h"

class WorldObject : public Displayable, public Positionable,
    public Rotatable, public Sizeable {
  public:
    WorldObject(Coord pos, Coord orientation, Coord size): pos(pos),
        orientation(orientation), size(size), destroyable(false) {}

    WorldObject(): WorldObject(Coord(), Coord(), Coord()) {}

    virtual ~WorldObject() {}

    virtual Coord &getPosition() override
    {
        return this->pos;
    }

    virtual Coord &getOrientation() override
    {
        return this->orientation;
    }

    virtual Coord &getSize() override
    {
        return this->size;
    }

    virtual Box getBoundingBox();

    virtual void move(const Coord &c);

    virtual void display() = 0;

    virtual void animate(double secsElapsed) {}

    virtual bool isDestroyable() const
    {
        return this->destroyable;
    }

    virtual void setDestroyable(bool b)
    {
        this->destroyable = b;
    }
  protected:
    Coord pos, orientation, size;

    bool destroyable;
};

#endif /* WORLDOBJECT_H */
