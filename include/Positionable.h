#ifndef POSITIONABLE_H
#define POSITIONABLE_H

#include "Coord.h"

class Positionable {
  public:
    virtual ~Positionable() {}

    virtual Coord &getPosition() = 0;
};

#endif /* POSITIONABLE_H */
