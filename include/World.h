#ifndef WORLD_H
#define WORLD_H

#include "Displayable.h"
#include "Positionable.h"
#include "Region.h"

#define REGIONS_X 20
#define REGIONS_Z 20

using std::make_shared;

class World : public Displayable {
  public:
    World()
    {
        for (unsigned int x = 0; x < REGIONS_X; ++x)
        {
            for (unsigned int z = 0; z < REGIONS_Z; ++z)
                this->regions[x][z] = make_shared<Region>(x, z);
        }
    }

    virtual ~World() {}

    shared_ptr<Region> getRegion(const unsigned int xInd, const unsigned int zInd)
    {
        return this->regions[xInd][zInd];
    }

    shared_ptr<Region> getRegion(const Coord &c)
    {
        unsigned int xInd = static_cast<unsigned int>(c.getX() / REGION_WIDTH);
        unsigned int zInd = static_cast<unsigned int>(c.getZ() / REGION_HEIGHT);

        // TODO: Index bounds checks

        return regions[xInd][zInd];
    }

    shared_ptr<Region> getRegion(shared_ptr<Positionable> entity)
    {
        return getRegion(entity->getPosition());
    }

    virtual shared_ptr<WorldObject> getObjectContaining(const Coord &c);

    void addObject(shared_ptr<WorldObject> obj)
    {
        // TODO: Check if the region actually exists

        getRegion(obj)->addObject(obj);
    }

    void removeObject(shared_ptr<WorldObject> obj)
    {
        getRegion(obj)->removeObject(obj);
    }

    virtual void display() override;

    virtual void animate(double secsElapsed);
  private:
    shared_ptr<Region> regions[REGIONS_X][REGIONS_Z];
};

#endif /* WORLD_H */
