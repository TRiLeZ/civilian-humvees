#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <memory>
#include "Camera.h"

using std::shared_ptr;
using std::make_shared;

class Viewport {
  public:
    Viewport(): camera(make_shared<Camera>()) {}

    virtual ~Viewport() {}

    virtual int getWidth() const
    {
        return this->width;
    }

    virtual int getHeight() const
    {
        return this->height;
    }

    virtual int getX() const
    {
        return this->x;
    }

    virtual int getY() const
    {
        return this->y;
    }

    virtual shared_ptr<Camera> getCamera() const
    {
        return this->camera;
    }

    virtual shared_ptr<Camera> getCamera()
    {
        return this->camera;
    }

    virtual void setSize(const int width, const int height)
    {
        this->width = width;
        this->height = height;

        getCamera()->viewportChanged(width, height);
    }

    virtual void setLocation(const int x, const int y)
    {
        this->x = x;
        this->y = y;
    }

    virtual void update()
    {
        glViewport(getX(), getY(), getWidth(), getHeight());
    }
  private:
    int x, y, width, height;

    shared_ptr<Camera> camera;
};

#endif /* VIEWPORT_H */
