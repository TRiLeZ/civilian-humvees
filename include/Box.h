#ifndef BOX_H
#define BOX_H

#include <algorithm>
#include "Coord.h"

class Box {
  public:
    Box(const Coord &pos, const Coord &size): Box(pos.getX(), pos.getY(),
                pos.getZ(), pos.getX() + size.getX(), pos.getY() + size.getY(),
                pos.getZ() + size.getZ()) {}

    Box(double x1, double y1, double z1, double x2, double y2, double z2)
    {
        double xMin = std::min(x1, x2);
        double xMax = std::max(x1, x2);

        double yMin = std::min(y1, y2);
        double yMax = std::max(y1, y2);

        double zMin = std::min(z1, z2);
        double zMax = std::max(z1, z2);

        this->pos = Coord(xMin, yMin, zMin);
        this->size = Coord(xMax - xMin, yMax - yMin, zMax - zMin);
    }

    virtual ~Box() {}

    virtual Coord getPosition() const
    {
        return this->pos;
    }

    virtual Coord &getPosition()
    {
        return this->pos;
    }

    virtual Coord getSize() const
    {
        return this->size;
    }

    virtual Coord &getSize()
    {
        return this->size;
    }

    virtual Coord getCenter()
    {
        return Coord(getPosition().getX() + (getSize().getX() / 2.0),
                     getPosition().getY() + (getSize().getY() / 2.0),
                     getPosition().getZ() + (getSize().getZ() / 2.0));
    }

    virtual bool contains(const Coord &c) const
    {
        return c.getX() >= getPosition().getX() && c.getY() >= getPosition().getY()
               && c.getZ() >= getPosition().getZ()
               && c.getX() < getPosition().getX() + getSize().getX()
               && c.getY() < getPosition().getY() + getSize().getY()
               && c.getZ() < getPosition().getZ() + getSize().getZ();
    }
  private:
    Coord pos, size;
};

#endif /* BOX_H */
