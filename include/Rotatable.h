#ifndef ROTATABLE_H
#define ROTATABLE_H

#include "Coord.h"

class Rotatable {
  public:
    virtual ~Rotatable() {}

    virtual Coord &getOrientation() = 0;
};

#endif /* ROTATABLE_H */
