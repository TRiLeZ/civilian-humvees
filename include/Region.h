#ifndef REGION_H
#define REGION_H

#include <list>
#include <memory>
#include "Displayable.h"
#include "WorldObject.h"

#define REGION_WIDTH 128.0
#define REGION_HEIGHT 128.0

using std::list;
using std::shared_ptr;

class Region : public Displayable {
  public:
    Region(const unsigned int xInd, const unsigned int zInd): xInd(xInd),
        zInd(zInd) {}

    Region(): Region(0, 0) {}

    virtual ~Region() {}

    virtual unsigned int getXInd() const
    {
        return this->xInd;
    }

    virtual unsigned int getZInd() const
    {
        return this->zInd;
    }

    virtual double getX() const
    {
        return getXInd() * getWidth();
    }

    virtual double getZ() const
    {
        return getZInd() * getHeight();
    }

    virtual double getWidth() const
    {
        return REGION_WIDTH;
    }

    virtual double getHeight() const
    {
        return REGION_HEIGHT;
    }

    virtual list<shared_ptr<WorldObject>> &getObjects()
    {
        return this->objects;
    }

    virtual shared_ptr<WorldObject> getObjectContaining(const Coord &c);

    virtual void addObject(shared_ptr<WorldObject> obj)
    {
        // TODO: Check if the object is actually in this region

        this->objects.push_back(obj);
    }

    virtual void removeObject(shared_ptr<WorldObject> obj)
    {
        auto remove = [obj](shared_ptr<WorldObject> o)
        {
            return o.get() == obj.get();
        };

        this->objects.remove_if(remove);
    }

    virtual bool operator==(const Region &region)
    {
        return this->getXInd() == region.getXInd()
               && this->getZInd() == region.getZInd();
    }

    virtual bool operator!=(const Region &region)
    {
        return !operator==(region);
    }

    virtual void display() override;

    virtual void animate(double secsElapsed);
  protected:
    virtual void displayGround();
  private:
    unsigned int xInd, zInd;

    list<shared_ptr<WorldObject>> objects;
};

#endif /* REGION_H */
