#ifndef REGIONPOPULATOR_H
#define REGIONPOPULATOR_H

#include "Region.h"

class RegionPopulator {
  public:
    virtual ~RegionPopulator() {}

    virtual void populate(shared_ptr<Region> region);
  protected:
    virtual void buildRoads(shared_ptr<Region> region);

    virtual void buildBuildings(shared_ptr<Region> region);

    virtual void buildBuilding(shared_ptr<Region> region, double x, double z,
                               double width, double height);
};

#endif /* REGIONPOPULATOR_H */
