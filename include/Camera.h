#ifndef CAMERA_H
#define CAMERA_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <memory>
#include "LookAt.h"
#include "WorldObject.h"

using std::shared_ptr;

class Camera {
  public:
    Camera(): eyeX(0.0), eyeY(0.0), eyeZ(0.0), centerX(0.0), centerY(0.0),
        centerZ(0.0), upX(0.0), upY(0.0), upZ(0.0), fovY(0.0), aspect(0.0), zNear(0.0),
        zFar(0.0) {}

    virtual ~Camera() {}

    virtual double getEyeX() const
    {
        return this->eyeX;
    }

    virtual double getEyeY() const
    {
        return this->eyeY;
    }

    virtual double getEyeZ() const
    {
        return this->eyeZ;
    }

    Coord getEye() const
    {
        return Coord(getEyeX(), getEyeY(), getEyeZ());
    }

    virtual double getCenterX() const
    {
        return this->centerX;
    }

    virtual double getCenterY() const
    {
        return this->centerY;
    }

    virtual double getCenterZ() const
    {
        return this->centerZ;
    }

    Coord getCenter() const
    {
        return Coord(getCenterX(), getCenterY(), getCenterZ());
    }

    virtual double getUpX() const
    {
        return this->upX;
    }

    virtual double getUpY() const
    {
        return this->upY;
    }

    virtual double getUpZ() const
    {
        return this->upZ;
    }

    Coord getUp() const
    {
        return Coord(getUpX(), getUpY(), getUpZ());
    }

    virtual double getFovY() const
    {
        return this->fovY;
    }

    virtual double getAspect() const
    {
        return this->aspect;
    }

    virtual double getZNear() const
    {
        return this->zNear;
    }

    virtual double getZFar() const
    {
        return this->zFar;
    }

    virtual void setEye(double x, double y, double z)
    {
        this->eyeX = x;
        this->eyeY = y;
        this->eyeZ = z;
    }

    virtual void setEye(const Coord &c)
    {
        this->eyeX = c.getX();
        this->eyeY = c.getY();
        this->eyeZ = c.getZ();
    }

    virtual void setCenter(double x, double y, double z)
    {
        this->centerX = x;
        this->centerY = y;
        this->centerZ = z;
    }

    virtual void setCenter(const Coord &c)
    {
        this->centerX = c.getX();
        this->centerY = c.getY();
        this->centerZ = c.getZ();
    }

    virtual void setUp(double x, double y, double z)
    {
        this->upX = x;
        this->upY = y;
        this->upZ = z;
    }

    virtual void setPerspective(double fovY, double zNear, double zFar)
    {
        this->fovY = fovY;
        this->zNear = zNear;
        this->zFar = zFar;
    }

    virtual void viewportChanged(double width, double height)
    {
        this->aspect = width / height;
    }

    virtual void update()
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(getFovY(), getAspect(), getZNear(), getZFar());
    }

    virtual void applyLookAt()
    {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        if (this->lookAt.get() != NULL)
        {
            setEye(this->lookAt->getEye());
            setCenter(this->lookAt->getCenter());
        }

        gluLookAt(getEyeX(), getEyeY(), getEyeZ(), getCenterX(), getCenterY(),
                  getCenterZ(), getUpX(), getUpY(), getUpZ());
    }

    virtual void setLookAt(shared_ptr<LookAt> at)
    {
        this->lookAt = at;
    }

    virtual shared_ptr<LookAt> getLookAt()
    {
        return this->lookAt;
    }
  private:
    double eyeX, eyeY, eyeZ;
    double centerX, centerY, centerZ;
    double upX, upY, upZ;

    double fovY, aspect, zNear, zFar;

    shared_ptr<LookAt> lookAt;
};

#endif /* CAMERA_H */
