#ifndef SPHEREBUILDING_H
#define SPHEREBUILDING_H

#include "../WorldObject.h"

class SphereBuilding : public WorldObject {
  public:
    SphereBuilding(Coord pos, Coord orientation, Coord size): WorldObject(pos,
                orientation, size) {}

    virtual ~SphereBuilding() {}

    virtual void display() override;

    virtual void animate(double secsElapsed) override;
  private:
    double animRotation = rand() % 360;
};

#endif /* SPHEREBUILDING_H */
