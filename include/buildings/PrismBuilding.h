#ifndef PRISMBUILDING_H
#define PRISMBUILDING_H

#include "../WorldObject.h"

class PrismBuilding : public WorldObject {
  public:
    PrismBuilding(Coord pos, Coord orientation, Coord size): WorldObject(pos,
                orientation, size) {}

    virtual ~PrismBuilding() {}

    virtual void display() override;
};

#endif /* PRISMBUILDING_H */
