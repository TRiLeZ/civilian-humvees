#ifndef CYLINDERBUILDING_H
#define CYLINDERBUILDING_H

#include "../WorldObject.h"

class CylinderBuilding : public WorldObject {
  public:
    CylinderBuilding(Coord pos, Coord orientation, Coord size): WorldObject(pos,
                orientation, size) {}

    virtual ~CylinderBuilding() {}

    virtual void display() override;

    virtual void animate(double secsElapsed) override;
  private:
    double animRotation = rand() % 360;
};

#endif /* CYLINDERBUILDING_H */
