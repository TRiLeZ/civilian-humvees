#ifndef MATH_H
#define MATH_H

#include <cmath>
#include <limits>
#include <iomanip>
#include <iostream>
#include <type_traits>
#include <algorithm>
#include "Coord.h"

// Credits: https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
template<class T>
typename std::enable_if < !std::numeric_limits<T>::is_integer,
         bool >::type almostEqual(T x, T y, int ulp)
{
    return std::fabs(x - y) <= std::numeric_limits<T>::epsilon() * std::fabs(
               x + y) * ulp || std::fabs(x - y) < std::numeric_limits<T>::min();
}

static double degToRad(double deg)
{
    static double halfPi = M_PI / 180.0;

    return deg * halfPi;
}

static double dotProduct(const Coord &a, const Coord &b)
{
    double product = 0.0;

    product = a.getX() * b.getX();
    product += a.getY() * b.getY();
    product += a.getZ() * b.getZ();

    return product;
}

static Coord crossProduct(const Coord &a, const Coord &b)
{
    Coord product;

    product.setX(a.getY() * b.getZ() - a.getZ() * b.getY());
    product.setY(a.getZ() * b.getX() - a.getX() * b.getZ());
    product.setZ(a.getX() * b.getY() - a.getY() * b.getX());

    return product;
}

// Credits: https://stackoverflow.com/a/42422624
static Coord rotate(const Coord &v, const Coord &axis, double deg)
{
    double theta = degToRad(deg);

    double cos_theta = cos(theta);
    double sin_theta = sin(theta);

    Coord rotated = (v * cos_theta) + (crossProduct(axis,
                                       v) * sin_theta) + (axis * dotProduct(axis, v)) * (1 - cos_theta);

    return rotated;
}

#endif /* MATH_H */
