#ifndef GLEXT_H
#define GLEXT_H

#include <cmath>
#include <GL/gl.h>

void drawCircle(double radius, double x, double y, double z)
{
    glBegin(GL_TRIANGLE_FAN);

    glVertex3d(x, y, z);

    for (unsigned int i = 0; i <= 360; ++i)
        glVertex3d(radius * cos(M_PI * i / 180.0) + x,
                   radius * sin(M_PI * i / 180.0) + y, z);

    glEnd();
}

#endif /* GLEXT_H */
