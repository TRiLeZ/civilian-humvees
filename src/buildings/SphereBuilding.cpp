#include <GL/gl.h>
#include <GL/glut.h>
#include <algorithm>
#include "buildings/SphereBuilding.h"

#define ANIMATION_SPEED 30.0

void SphereBuilding::display()
{
    Coord size = getSize();

    double radius = std::min(size.getX(), size.getY()) * 0.5;
    double antennaBase = radius * 0.25;
    double antennaHeight = antennaBase * 1.5;

    // Set the position
    glTranslated(size.getX() / 2.0, radius, size.getZ() / 2.0);

    // Green sphere
    glColor4f(0.296875f, 0.68359375f, 0.3125f, 1.0f);

    glutSolidSphere(radius, 20, 20);

    // Apply animation - rotation of antenna
    glRotated(this->animRotation, 0.0, 1.0, 0.0);

    // Give the antenna a tilt
    glRotated(10.0, 1.0, 0.0, 0.0);

    glTranslated(0.0, radius, 0.0);

    // Darker green antenna
    glColor4f(0.10546875f, 0.3671875f, 0.125f, 1.0f);

    // Rotate the antenna upwards
    glRotated(-90.0, 1.0, 0.0, 0.0);

    glutSolidCone(antennaBase, antennaHeight, 15, 15);
}

void SphereBuilding::animate(double secsElapsed)
{
    this->animRotation += ANIMATION_SPEED * secsElapsed;
}
