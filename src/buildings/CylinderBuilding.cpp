#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "buildings/CylinderBuilding.h"

#define ANIMATION_SPEED 15.0

void CylinderBuilding::display()
{
    GLUquadric *quad = gluNewQuadric();

    if (quad == 0) // Not enough memory
        return;

    Coord size = getSize();

    double cubeHeight = size.getY() * 0.2;

    double cubeSize = std::min(size.getX(), size.getZ()) / 2.0;

    double cylinderRadius = std::min(size.getX(), size.getZ()) / 4.0;
    double cylinderHeight = size.getY() - cubeHeight;

    // Set the position
    glTranslated(size.getX() / 2.0, cylinderHeight, size.getZ() / 2.0);

    // Rotate the cylinder upwards
    glRotated(90.0, 1.0, 0.0, 0.0);

    // Red cylinder
    glColor4f(0.953125f, 0.26171875f, 0.2109375f, 1.0f);

    gluCylinder(quad, cylinderRadius, cylinderRadius, cylinderHeight, 15, 15);

    glRotated(this->animRotation, 0.0, 0.0, 1.0);

    // Darker red cube
    glColor4f(0.82421875f, 0.18359375f, 0.18359375f, 1.0f);

    glutSolidCube(cubeSize);

    gluDeleteQuadric(quad);
}

void CylinderBuilding::animate(double secsElapsed)
{
    this->animRotation += ANIMATION_SPEED * secsElapsed;
}
