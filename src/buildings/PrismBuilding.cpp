#include <GL/gl.h>
#include "buildings/PrismBuilding.h"

void PrismBuilding::display()
{
    glBegin(GL_QUADS);

    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

    double x2 = x + getSize().getX();
    double y2 = y + getSize().getY();
    double z2 = z + getSize().getZ();

    double cx = x + (getSize().getX() / 2.0);
    double cy = y + (getSize().getY() / 2.0);
    double cz = z + (getSize().getZ() / 2.0);

    // Bottom face, teal
    glColor4f(0.0f, 1.0f, 1.0f, 1.0f);

    glVertex3d(x, y, z2);
    glVertex3d(x, y, z);
    glVertex3d(x2, y, z);
    glVertex3d(x2, y, z2);

    // Back face, green
    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);

    glVertex3d(x, y2, z);
    glVertex3d(x2, y2, z);
    glVertex3d(x2, y, z);
    glVertex3d(x, y, z);

    // Time to add some windows
    glColor4f(0.5625f, 0.7890625f, 0.97265625f, 1.0f);

    glVertex3d(cx - 5, cy + 5, z - 0.1);
    glVertex3d(cx + 5, cy + 5, z - 0.1);
    glVertex3d(cx + 5, cy - 5, z - 0.1);
    glVertex3d(cx - 5, cy - 5, z - 0.1);

    // Right face, blue
    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);

    glVertex3d(x2, y, z);
    glVertex3d(x2, y2, z);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y, z2);

    // Time to add some windows
    glColor4f(0.5625f, 0.7890625f, 0.97265625f, 1.0f);

    glVertex3d(x2 + 0.1, cy - 5, cz - 5);
    glVertex3d(x2 + 0.1, cy + 5, cz - 5);
    glVertex3d(x2 + 0.1, cy + 5, cz + 5);
    glVertex3d(x2 + 0.1, cy - 5, cz + 5);

    // Front face, red
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

    glVertex3d(x, y, z2);
    glVertex3d(x2, y, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x, y2, z2);

    // Time to add some windows
    glColor4f(0.5625f, 0.7890625f, 0.97265625f, 1.0f);

    glVertex3d(cx - 5, cy - 5, z2 + 0.1);
    glVertex3d(cx + 5, cy - 5, z2 + 0.1);
    glVertex3d(cx + 5, cy + 5, z2 + 0.1);
    glVertex3d(cx - 5, cy + 5, z2 + 0.1);

    // Left face, yellow
    glColor4f(1.0f, 1.0f, 0.0f, 1.0f);

    glVertex3d(x, y, z);
    glVertex3d(x, y, z2);
    glVertex3d(x, y2, z2);
    glVertex3d(x, y2, z);

    // Time to add some windows
    glColor4f(0.5625f, 0.7890625f, 0.97265625f, 1.0f);

    glVertex3d(x - 0.1, cy - 5, cz - 5);
    glVertex3d(x - 0.1, cy - 5, cz + 5);
    glVertex3d(x - 0.1, cy + 5, cz + 5);
    glVertex3d(x - 0.1, cy + 5, cz - 5);

    // Top face, pink
    glColor4f(1.0f, 0.0f, 1.0f, 1.0f);

    glVertex3d(x, y2, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y2, z);
    glVertex3d(x, y2, z);

    glEnd();
}
