#include "World.h"

void World::display()
{
    // TODO: Only display objects from the loaded region

    for (unsigned int x = 0; x < REGIONS_X; ++x)
    {
        for (unsigned int z = 0; z < REGIONS_Z; ++z)
        {
            shared_ptr<Region> region = getRegion(x, z);

            region->display();
        }
    }
}

void World::animate(double secsElapsed)
{
    // TODO: Only animate objects from the loaded region

    for (unsigned int x = 0; x < REGIONS_X; ++x)
    {
        for (unsigned int z = 0; z < REGIONS_Z; ++z)
        {
            shared_ptr<Region> region = getRegion(x, z);

            region->animate(secsElapsed);
        }
    }
}

shared_ptr<WorldObject> World::getObjectContaining(const Coord &c)
{
    shared_ptr<Region> region = getRegion(c);

    if (region.get() == NULL)
        return shared_ptr<WorldObject>(NULL);

    return region->getObjectContaining(c);
}
