#include <GL/gl.h>
#include <GL/glut.h>
#include "Car.h"
#include "util/GLExt.h"

#define ANIMATION_SPEED 61.0

void Car::display()
{
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

    double x2 = x + getSize().getX();
    double y2 = y + getSize().getY();
    double z2 = z + getSize().getZ();

    double wheelSize = getSize().getZ() / 7;

    double lightSize = getSize().getX() / 14;

    double wheelZ1 = (getSize().getZ() / 7) * 2;
    double wheelZ2 = (getSize().getZ() / 7) * 5;

    double lightX1 = (getSize().getX() / 7) * 1;
    double lightX2 = (getSize().getX() / 7) * 6;

    double plateX1 = (getSize().getX() / 7) * 2.5;
    double plateX2 = (getSize().getX() / 7) * 4.5;

    double plateY1 = (getSize().getY() / 7) * 1.5;
    double plateY2 = (getSize().getY() / 7) * 3;

    displayBody(x, y, z, x2, y + (y2 - y) / 2.0, z2);
    displayTop(x, y, z + (z2 - z) / 4.0, x2, y2, z2 - (z2 - z) / 4.0);

    // Grey license plate
    glColor4f(0.3f, 0.3f, 0.3f, 1.0f);

    glBegin(GL_QUADS);

    // Front plate
    glVertex3d(plateX1, plateY1, z2 + 0.1);
    glVertex3d(plateX2, plateY1, z2 + 0.1);
    glVertex3d(plateX2, plateY2, z2 + 0.1);
    glVertex3d(plateX1, plateY2, z2 + 0.1);

    // Back plate
    glVertex3d(plateX1, plateY1, z - 0.1);
    glVertex3d(plateX1, plateY2, z - 0.1);
    glVertex3d(plateX2, plateY2, z - 0.1);
    glVertex3d(plateX2, plateY1, z - 0.1);

    glEnd();

    if (getRenderAccessories())
    {
        // Black antenna
        glColor4f(0.1f, 0.1f, 0.1f, 1.0f);

        glPushMatrix();
        glTranslated(x + (x2 - x) / 5.0, y + (y2 - y) / 2.0 + 0.5, z + 0.5);
        glScaled(0.5, 1.0, 0.5);
        glRotated(this->animRotation, 0.0, 1.0, 0.0);
        glutWireIcosahedron();
        glPopMatrix();

        double rocketSize = 1.0;
        double rocketLen = getSize().getZ() / 4;

        displayRocket(x - rocketSize, getSize().getY() / 2.0 - 1, z2 - rocketLen, x,
                      getSize().getY() / 2.0, z2);

        displayRocket(x2, getSize().getY() / 2.0 - 1, z2 - rocketLen, x2 + rocketSize,
                      getSize().getY() / 2.0, z2);
    }

    // Red brakelights
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

    // Brakelight 1
    glPushMatrix();
    glTranslated(lightX1, wheelSize, z - 0.1);
    glRotated(180.0, 0.0, 1.0, 0.0);
    drawCircle(lightSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Brakelight 2
    glPushMatrix();
    glTranslated(lightX2, wheelSize, z - 0.1);
    glRotated(180.0, 0.0, 1.0, 0.0);
    drawCircle(lightSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Yellow headlights
    glColor4f(1.0f, 1.0f, 0.0f, 1.0f);

    // Headlight 1
    glPushMatrix();
    glTranslated(lightX1, wheelSize, z2 + 0.1);
    drawCircle(lightSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Headlight 2
    glPushMatrix();
    glTranslated(lightX2, wheelSize, z2 + 0.1);
    drawCircle(lightSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Move wheels up
    glTranslated(0.0, wheelSize / 2.0, 0.0);

    // Black wheels
    glColor4f(0.1, 0.1, 0.1, 1.0);

    // Draw wheel 1
    glPushMatrix();
    glTranslated(x - 0.1, 0.0, wheelZ1);
    glRotated(-90, 0.0, 1.0, 0.0);
    drawCircle(wheelSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Draw wheel 2
    glPushMatrix();
    glTranslated(x - 0.1, 0.0, wheelZ2);
    glRotated(-90, 0.0, 1.0, 0.0);
    drawCircle(wheelSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Draw wheel 3
    glPushMatrix();
    glTranslated(x2 + 0.1, 0.0, wheelZ1);
    glRotated(90, 0.0, 1.0, 0.0);
    drawCircle(wheelSize, 0.0, 0.0, 0.0);
    glPopMatrix();

    // Draw wheel 4
    glPushMatrix();
    glTranslated(x2 + 0.1, 0.0, wheelZ2);
    glRotated(90, 0.0, 1.0, 0.0);
    drawCircle(wheelSize, 0.0, 0.0, 0.0);
    glPopMatrix();
}

void Car::displayRocket(double x1, double y1, double z1, double x2, double y2,
                        double z2)
{
    glBegin(GL_QUADS);

    // Grey rocket launchers
    glColor4f(0.6f, 0.6f, 0.6f, 1.0f);

    // Bottom face
    glVertex3d(x1, y1, z2);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y1, z2);

    // Back face
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y1, z1);
    glVertex3d(x1, y1, z1);

    // Right face
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y1, z2);

    // Front face
    glVertex3d(x1, y1, z2);
    glVertex3d(x2, y1, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x1, y2, z2);

    // Left face
    glVertex3d(x1, y1, z1);
    glVertex3d(x1, y1, z2);
    glVertex3d(x1, y2, z2);
    glVertex3d(x1, y2, z1);

    // Top face
    glVertex3d(x1, y2, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y2, z1);
    glVertex3d(x1, y2, z1);

    glEnd();
}

void Car::displayBody(double x1, double y1, double z1, double x2, double y2,
                      double z2)
{
    glBegin(GL_QUADS);

    // Purple car body
    glColor4f(0.5f, 0.5f, 1.0f, 1.0f);

    // Bottom face
    glVertex3d(x1, y1, z2);
    glVertex3d(x1, y1, z1);
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y1, z2);

    // Back face
    glVertex3d(x1, y2, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y1, z1);
    glVertex3d(x1, y1, z1);

    // Right face
    glVertex3d(x2, y1, z1);
    glVertex3d(x2, y2, z1);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y1, z2);

    // Front face
    glVertex3d(x1, y1, z2);
    glVertex3d(x2, y1, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x1, y2, z2);

    // Left face
    glVertex3d(x1, y1, z1);
    glVertex3d(x1, y1, z2);
    glVertex3d(x1, y2, z2);
    glVertex3d(x1, y2, z1);

    // Top face
    glVertex3d(x1, y2, z2);
    glVertex3d(x2, y2, z2);
    glVertex3d(x2, y2, z1);
    glVertex3d(x1, y2, z1);

    glEnd();
}

void Car::displayTop(double x1, double y1, double z1, double x2, double y2,
                     double z2)
{
    displayBody(x1, y1, z1, x2, y2, z2);

    glBegin(GL_QUADS);

    // Blue window
    glColor4f(0.5625f, 0.7890625f, 0.97265625f, 1.0f);

    double frontX1 = x1 + (x2 - x1) / 10.0;
    double frontX2 = x2 - (x2 - x1) / 10.0;

    double sizeZ1 = z1 + (z2 - z1) / 10.0;
    double sizeZ2 = z2 - (z2 - z1) / 10.0;

    double frontY1 = y1 + (y2 - y1) / 2.0;
    double frontY2 = y2 - (y2 - y1) / 10.0;

    // Front window
    glVertex3d(frontX1, frontY1, z2 + 0.1);
    glVertex3d(frontX2, frontY1, z2 + 0.1);
    glVertex3d(frontX2, frontY2, z2 + 0.1);
    glVertex3d(frontX1, frontY2, z2 + 0.1);

    // Back window
    glVertex3d(frontX1, frontY1, z1 - 0.1);
    glVertex3d(frontX1, frontY2, z1 - 0.1);
    glVertex3d(frontX2, frontY2, z1 - 0.1);
    glVertex3d(frontX2, frontY1, z1 - 0.1);

    // Right window
    glVertex3d(x1 - 0.1, frontY1, sizeZ1);
    glVertex3d(x1 - 0.1, frontY1, sizeZ2);
    glVertex3d(x1 - 0.1, frontY2, sizeZ2);
    glVertex3d(x1 - 0.1, frontY2, sizeZ1);

    // left window
    glVertex3d(x2 + 0.1, frontY1, sizeZ1);
    glVertex3d(x2 + 0.1, frontY2, sizeZ1);
    glVertex3d(x2 + 0.1, frontY2, sizeZ2);
    glVertex3d(x2 + 0.1, frontY1, sizeZ2);

    glEnd();
}

void Car::animate(double secsElapsed)
{
    this->animRotation += ANIMATION_SPEED * secsElapsed;
}
