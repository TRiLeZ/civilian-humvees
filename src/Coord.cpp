#include "Coord.h"
#include "util/Math.h"

bool Coord::operator==(const Coord &c) const
{
    return almostEqual(getX(), c.getX(), 2) && almostEqual(getY(), c.getY(), 2)
           && almostEqual(getZ(), c.getZ(), 2);
}

Coord Coord::operator*(double d) const
{
    Coord c = Coord(getX() * d, getY() * d, getZ() * d);

    return c;
}

Coord Coord::operator+(const Coord &c) const
{
    Coord c2 = Coord(getX() + c.getX(), getY() + c.getY(), getZ() + c.getZ());

    return c2;
}

Coord Coord::operator-(const Coord &c) const
{
    return *this + (-c);
}

Coord Coord::operator-() const
{
    Coord c2 = Coord(-getX(), -getY(), -getZ());

    return c2;
}
