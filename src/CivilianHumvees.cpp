#define PROGRAM_TITLE "Civilian Humvees"
#define DEFAULT_WIDTH 600
#define DEFAULT_HEIGHT 400
#define KEY_PRESSED_RANGE 256
#define CAR_MOVEMENT_SPEED 0.05

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <memory>
#include "Car.h"
#include "FollowingView.h"
#include "RegionPopulator.h"
#include "Road.h"
#include "Window.h"
#include "World.h"

using std::shared_ptr;
using std::make_shared;

shared_ptr<Window> window;
shared_ptr<World> world;
shared_ptr<LookAt> lookAt;
shared_ptr<Car> myCar;

struct timeval loopCycle, lastLoopCycle, lastAnimationCycle;

bool keyPressed[KEY_PRESSED_RANGE];

bool paused = false;

const Coord myCarOrigin = Coord((REGIONS_X / 2.0 * REGION_WIDTH) +
                                REGION_WIDTH / 2.0 - CAR_WIDTH / 2.0, 1.0,
                                (REGIONS_Z / 2.0 * REGION_HEIGHT) + REGION_HEIGHT / 2.0 - CAR_HEIGHT / 2);

void displayWorld()
{
    window->getViewport()->getCamera()->applyLookAt();

    world->display();
}

bool isMyCarInBounds()
{
    Coord pos = myCar->getBoundingBox().getCenter();

    shared_ptr<Region> finalRegion = world->getRegion(REGIONS_X - 1, REGIONS_Z - 1);

    double maxX = finalRegion->getX() + finalRegion->getWidth();
    double maxZ = finalRegion->getZ() + finalRegion->getHeight();

    return pos.getX() >= 0 && pos.getZ() >= 0 && pos.getX() < maxX
           && pos.getZ() < maxZ;
}

bool isMyCarNearBounds()
{
    Box bounds = myCar->getBoundingBox();

    shared_ptr<Region> finalRegion = world->getRegion(REGIONS_X - 1, REGIONS_Z - 1);

    double maxX = finalRegion->getX() + finalRegion->getWidth();
    double maxZ = finalRegion->getZ() + finalRegion->getHeight();

    return bounds.getPosition().getX() <= 0 || bounds.getPosition().getZ() <= 0
           || (bounds.getPosition() + bounds.getSize()).getX() >= maxX
           || (bounds.getPosition() + bounds.getSize()).getZ() >= maxZ;
}

bool isMyCarAtIntersection()
{
    Coord center = myCar->getBoundingBox().getCenter();

    shared_ptr<Region> region = world->getRegion(myCar);

    double x1 = region->getX() + (region->getWidth() / 2.0) - (ROAD_SIZE / 2.0);
    double z1 = region->getZ() + (region->getHeight() / 2.0) - (ROAD_SIZE / 2.0);

    double x2 = x1 + ROAD_SIZE;
    double z2 = z1 + ROAD_SIZE;

    return center.getX() >= x1 && center.getZ() >= z1 && center.getX() < x2
           && center.getZ() < z2;
}

void handleMyCarMovement(double secsElapsed)
{
    shared_ptr<Region> startingRegion = world->getRegion(myCar);

    if (keyPressed[static_cast<unsigned int>('a')])
    {
        // Move our car forward
        Coord move = Coord(0.0, 0.0, CAR_MOVEMENT_SPEED / secsElapsed);

        myCar->move(move);

        // Move back if not in bounds
        if (!isMyCarInBounds())
            myCar->move(-move);
    }

    if (keyPressed[static_cast<unsigned int>('z')])
    {
        // Move our car forward
        Coord move = Coord(0.0, 0.0, -CAR_MOVEMENT_SPEED / secsElapsed);

        myCar->move(move);

        // Move back if not in bounds
        if (!isMyCarInBounds())
            myCar->move(-move);
    }

    shared_ptr<Region> endingRegion = world->getRegion(myCar);

    if (startingRegion.get() != endingRegion.get())
    {
        startingRegion->removeObject(myCar);
        endingRegion->addObject(myCar);
    }
}

void handleMovement(double secsElapsed)
{
    handleMyCarMovement(secsElapsed);
}

void handleAnimations(double secsElapsed)
{
    world->animate(secsElapsed);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gettimeofday(&loopCycle, NULL);

    bool updateAnimCycle = false;

    if (!paused)
    {
        double secsElapsed = (double)(loopCycle.tv_usec - lastLoopCycle.tv_usec) /
                             1000000 + (double)(loopCycle.tv_sec - lastLoopCycle.tv_sec);

        double animSecsElapsed = (double)(loopCycle.tv_usec -
                                          lastAnimationCycle.tv_usec) /
                                 1000000 + (double)(loopCycle.tv_sec - lastAnimationCycle.tv_sec);

        handleMovement(secsElapsed);

        // Only animate every 10ms
        if (animSecsElapsed > 0.01)
        {
            updateAnimCycle = true;

            handleAnimations(animSecsElapsed);
        }

        std::fill(keyPressed, keyPressed + KEY_PRESSED_RANGE, false);
    }

    displayWorld();

    glutSwapBuffers();

    lastLoopCycle = loopCycle;

    if (updateAnimCycle)
        lastAnimationCycle = loopCycle;
}

void idle()
{
    // Nothing to do here, so just re-display
    display();
}

void reshape(int width, int height)
{
    window->getViewport()->setSize(width, height);

    // Update the GL viewport
    window->getViewport()->update();

    // Set the GL perspective and lookAt
    window->getViewport()->getCamera()->update();

    window->setSize(width, height);
}

void destroyBuildingAt(int x, int y)
{
    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);

    GLdouble modelview[16];
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

    GLdouble projection[16];
    glGetDoublev(GL_PROJECTION_MATRIX, projection);

    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;

    winX = static_cast<GLfloat>(x);
    winY = static_cast<GLfloat>(viewport[3]) - static_cast<GLfloat>(y);
    winZ = 0.0f;

    gluUnProject(winX, winY, winZ, modelview, projection, viewport, &posX, &posY,
                 &posZ);

    Coord pos = Coord(posX, posY, posZ);

    // Let's do some ray tracing

    auto camera = window->getViewport()->getCamera();

    Coord rayDir = pos - camera->getEye();

    double rayDist = std::sqrt(std::pow(rayDir.getX(), 2) + std::pow(rayDir.getY(),
                               2) + std::pow(rayDir.getZ(), 2));

    // Make rayDir a unit vector
    rayDir = rayDir * (1.0 / rayDist);

    double max = camera->getZFar() - camera->getZNear();

    for (unsigned int i = 0; i < max; ++i)
    {
        shared_ptr<WorldObject> obj = world->getObjectContaining(pos + (rayDir * i));

        if (obj.get() != NULL && obj.get() != myCar.get())
        {
            if (obj->isDestroyable())
                world->removeObject(obj);

            return;
        }
    }
}

void mouseEvent(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        // Destroy the object which was clicked, if possible
        destroyBuildingAt(x, y);
    }
}

void keyboardEvent(unsigned char c, int x, int y)
{
    unsigned int key = static_cast<int>(c);

    if (key >= 0 && key < KEY_PRESSED_RANGE)
        keyPressed[key] = true;

    switch (c)
    {
        case 'p':
            if (paused)
            {
                // Un-pause
                lastLoopCycle = loopCycle;
                lastAnimationCycle = loopCycle;
            }

            paused = !paused;

            break;

        case 'r':

            // Reset car position to origin
            if (!paused && isMyCarNearBounds())
            {
                // Remove from old region
                world->removeObject(myCar);

                myCar->getPosition().setLocation(myCarOrigin);

                // Add to new region
                world->addObject(myCar);
            }

            break;

        case 'q':

            // Turn left
            if (isMyCarAtIntersection())
                myCar->getOrientation().setY(myCar->getOrientation().getY() + 90.0);

            break;

        case 'w':

            // Turn right
            if (isMyCarAtIntersection())
                myCar->getOrientation().setY(myCar->getOrientation().getY() - 90.0);

            break;
    }
}

void specialKeyboardEvent(int key, int x, int y)
{
    if (lookAt.get() == NULL)
        return;

    Coord &rotation = lookAt->getRotation();
    Coord &centerOffset = lookAt->getCenterOffset();
    Coord &eyeOffset = lookAt->getEyeOffset();

    switch (key)
    {
        case GLUT_KEY_F1:
            // Look forward
            rotation.setY(0);

            break;

        case GLUT_KEY_F2:
            // Look right
            rotation.setY(rotation.getY() - 1.0);

            break;

        case GLUT_KEY_F3:
            // Look left
            rotation.setY(rotation.getY() + 1.0);

            break;

        case GLUT_KEY_F4:
            // Default view
            rotation.setLocation(0.0, 0.0, 0.0);
            centerOffset.setLocation(0.0, 0.0, 0.0);
            eyeOffset.setLocation(0.0, 0.0, 0.0);
            myCar->setRenderAccessories(false);

            break;

        case GLUT_KEY_F5:
            eyeOffset.setLocation(-CAR_WIDTH * 4, CAR_DEPTH * 4, -CAR_HEIGHT * 4);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F6:
            eyeOffset.setLocation(-CAR_WIDTH * 4, CAR_DEPTH * 4, CAR_HEIGHT * 4);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F7:
            eyeOffset.setLocation(CAR_WIDTH * 4, CAR_DEPTH * 4, CAR_HEIGHT * 4);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F8:
            eyeOffset.setLocation(CAR_WIDTH * 4, CAR_DEPTH * 4, -CAR_HEIGHT * 4);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F9:
            eyeOffset.setLocation(-CAR_WIDTH * 8, CAR_DEPTH * 32, -CAR_HEIGHT * 8);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F10:
            eyeOffset.setLocation(-CAR_WIDTH * 8, CAR_DEPTH * 32, CAR_HEIGHT * 8);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F11:
            eyeOffset.setLocation(CAR_WIDTH * 8, CAR_DEPTH * 32, CAR_HEIGHT * 8);
            myCar->setRenderAccessories(true);

            break;

        case GLUT_KEY_F12:
            eyeOffset.setLocation(CAR_WIDTH * 8, CAR_DEPTH * 32, -CAR_HEIGHT * 8);
            myCar->setRenderAccessories(true);

            break;
    }
}

void initCamera()
{
    shared_ptr<Camera> camera = window->getViewport()->getCamera();

    camera->setEye(myCarOrigin.getX() - 0.5, myCarOrigin.getY() + 128.0,
                   myCarOrigin.getZ());
    camera->setCenter(myCarOrigin.getX(), myCarOrigin.getY(), myCarOrigin.getZ());
    camera->setUp(0.0, 1.0, 0.0);
    camera->setPerspective(45.0, 1.0, 1024.0);
}

void populateRegions()
{
    RegionPopulator populator;

    for (unsigned int x = 0; x < REGIONS_X; ++x)
    {
        for (unsigned int z = 0; z < REGIONS_Z; ++z)
            populator.populate(world->getRegion(x, z));
    }
}

void buildCar()
{
    Coord pos = myCarOrigin;
    Coord orientation = Coord();
    Coord size = Coord(CAR_WIDTH, CAR_DEPTH, CAR_HEIGHT);

    myCar = make_shared<Car>(pos, orientation, size);

    world->getRegion(myCar)->addObject(myCar);

    lookAt = make_shared<FollowingView>(myCar);

    window->getViewport()->getCamera()->setLookAt(lookAt);
}

void initWorld()
{
    world = make_shared<World>();

    populateRegions();
    buildCar();
}

void init()
{
    std::fill(keyPressed, keyPressed + KEY_PRESSED_RANGE, false);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0);

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    srand(time(NULL));

    initCamera();
    initWorld();

    reshape(window->getWidth(), window->getHeight());

    gettimeofday(&loopCycle, NULL);
    gettimeofday(&lastLoopCycle, NULL);
    gettimeofday(&lastAnimationCycle, NULL);
}

void registerCallbacks()
{
    glutDisplayFunc(&display);
    glutIdleFunc(&idle);
    glutReshapeFunc(&reshape);
    glutMouseFunc(&mouseEvent);
    glutKeyboardFunc(&keyboardEvent);
    glutSpecialFunc(&specialKeyboardEvent);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);

    int id = glutCreateWindow(PROGRAM_TITLE);

    window = make_shared<Window>(id, DEFAULT_WIDTH, DEFAULT_HEIGHT);

    registerCallbacks();

    init();

    glutMainLoop();

    return 0;
}
