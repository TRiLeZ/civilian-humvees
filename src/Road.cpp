#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "Road.h"

void Road::display()
{
    glBegin(GL_QUADS);

    // Dark grey for road
    glColor4f(0.258f, 0.258f, 0.258f, 1.0f);

    glVertex3d(0.0, 0.0, getSize().getZ());
    glVertex3d(getSize().getX(), 0.0, getSize().getZ());
    glVertex3d(getSize().getX(), 0.0, 0.0);
    glVertex3d(0.0, 0.0, 0.0);

    // Yellow for road lines
    glColor4f(1.0f, 1.0f, 0.0f, 1.0f);

    double x = (getSize().getX() / 2.0) - (ROAD_LINE_SIZE / 2.0);

    double lineSegmentHeight = getSize().getZ() / ROAD_LINE_COUNT;
    double lineHeight = lineSegmentHeight / 2.0;

    for (unsigned int i = 0; i < ROAD_LINE_COUNT; ++i)
    {
        double z = i * lineSegmentHeight;

        glVertex3d(x, 0.01, z + lineHeight);
        glVertex3d(x + ROAD_LINE_SIZE, 0.01, z + lineHeight);
        glVertex3d(x + ROAD_LINE_SIZE, 0.01, z);
        glVertex3d(x, 0.01, z);
    }

    glEnd();
}
