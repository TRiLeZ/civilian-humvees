#include <stdlib.h>
#include "buildings/CylinderBuilding.h"
#include "buildings/PrismBuilding.h"
#include "buildings/SphereBuilding.h"
#include "RegionPopulator.h"
#include "Road.h"

using std::make_shared;

void RegionPopulator::populate(shared_ptr<Region> region)
{
    buildRoads(region);
    buildBuildings(region);
}

void RegionPopulator::buildRoads(shared_ptr<Region> region)
{
    Coord pos = Coord(region->getX() + (region->getWidth() / 2.0) -
                      (ROAD_SIZE / 2.0), ROAD_DEPTH_OFFSET, region->getZ());

    Coord vRot;
    Coord hRot = Coord(0.0, 90.0, 0.0);

    Coord size = Coord(ROAD_SIZE, ROAD_DEPTH, region->getHeight());

    shared_ptr<WorldObject> verticalRoad = make_shared<Road>(pos, vRot, size);
    shared_ptr<WorldObject> horizontalRoad = make_shared<Road>(pos, hRot, size);

    region->addObject(verticalRoad);
    region->addObject(horizontalRoad);
}

void RegionPopulator::buildBuildings(shared_ptr<Region> region)
{
    static double plotWidth = (REGION_WIDTH / 2.0) - (ROAD_SIZE / 2.0);
    static double plotHeight = (REGION_HEIGHT / 2.0) - (ROAD_SIZE / 2.0);

    static double sidewalkWidth = plotWidth * 0.1;
    static double sidewalkHeight = plotHeight * 0.1;

    static double buildingWidth = plotWidth - sidewalkWidth;
    static double buildingHeight = plotHeight - sidewalkHeight;

    static double plot1X = 0.0;
    static double plot1Z = 0.0;

    static double plot2X = 0.0;
    static double plot2Z = REGION_HEIGHT - plotHeight + sidewalkHeight;

    static double plot3X = REGION_WIDTH - plotWidth + sidewalkWidth;
    static double plot3Z = REGION_HEIGHT - plotHeight + sidewalkHeight;

    static double plot4X = REGION_WIDTH - plotWidth + sidewalkWidth;
    static double plot4Z = 0.0;

    // Gaps between buildings
    static double gapX = 10.0;
    static double gapZ = 10.0;

    buildBuilding(region, plot1X + gapX, plot1Z + gapZ, buildingWidth - gapX,
                  buildingHeight - gapZ);
    buildBuilding(region, plot2X + gapX, plot2Z, buildingWidth - gapX,
                  buildingHeight - gapZ);
    buildBuilding(region, plot3X, plot3Z, buildingWidth - gapX,
                  buildingHeight - gapZ);
    buildBuilding(region, plot4X, plot4Z + gapZ, buildingWidth - gapX,
                  buildingHeight - gapZ);
}

void RegionPopulator::buildBuilding(shared_ptr<Region> region, double x,
                                    double z, double width, double height)
{
    Coord pos(region->getX(), 0.0, region->getZ());
    Coord orientation;
    Coord size(width, 30.0 + (rand() % 70), height);

    pos.translate(x, 0.0, z);

    shared_ptr<WorldObject> building;

    double typeRand = rand() % 100;

    if (typeRand < 33)
        building = make_shared<PrismBuilding>(pos, orientation, size);
    else if (typeRand < 66)
        building = make_shared<CylinderBuilding>(pos, orientation, size);
    else
        building = make_shared<SphereBuilding>(pos, orientation, size);

    if (rand() % 100 < 50)
        building->setDestroyable(true);

    region->addObject(building);
}
