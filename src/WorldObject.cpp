#include "util/Math.h"
#include "WorldObject.h"

void WorldObject::move(const Coord &c)
{
    Coord rotated = c;

    rotated = rotate(rotated, Coord(1.0, 0.0, 0.0), getOrientation().getX());
    rotated = rotate(rotated, Coord(0.0, 1.0, 0.0), getOrientation().getY());
    rotated = rotate(rotated, Coord(0.0, 0.0, 1.0), getOrientation().getZ());

    getPosition().translate(rotated);
}

Box WorldObject::getBoundingBox()
{
    Coord pos1 = getPosition();
    Coord pos2 = pos1 + getSize();

    Coord center = pos1 + (getSize() * 0.5);

    pos1 = pos1 - center;

    // Rotate pos1 about center
    pos1 = rotate(pos1, Coord(1.0, 0.0, 0.0), getOrientation().getX());
    pos1 = rotate(pos1, Coord(0.0, 1.0, 0.0), getOrientation().getY());
    pos1 = rotate(pos1, Coord(0.0, 0.0, 1.0), getOrientation().getZ());

    pos1 = pos1 + center;

    pos2 = pos2 - center;

    // Rotate pos2 about center
    pos2 = rotate(pos2, Coord(1.0, 0.0, 0.0), getOrientation().getX());
    pos2 = rotate(pos2, Coord(0.0, 1.0, 0.0), getOrientation().getY());
    pos2 = rotate(pos2, Coord(0.0, 0.0, 1.0), getOrientation().getZ());

    pos2 = pos2 + center;

    Box box = Box(pos1, Coord(pos2.getX() - pos1.getX(), pos2.getY() - pos1.getY(),
                              pos2.getZ() - pos1.getZ()));

    return box;
}
