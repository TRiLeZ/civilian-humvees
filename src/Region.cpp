#include <GL/gl.h>
#include "Region.h"

void Region::displayGround()
{
    glPushMatrix();

    glColor4f(0.5f, 0.5f, 0.5f, 1.0f);

    glBegin(GL_QUADS);

    glVertex3d(getX(), 0.0, getZ());
    glVertex3d(getX(), 0.0, getZ() + getHeight());
    glVertex3d(getX() + getWidth(), 0.0, getZ() + getHeight());
    glVertex3d(getX() + getWidth(), 0.0, getZ());

    glEnd();

    glPopMatrix();
}

void Region::display()
{
    displayGround();

    for (shared_ptr<WorldObject> obj : getObjects())
    {
        glPushMatrix();

        Coord pos = obj->getPosition();
        Coord rotate = obj->getOrientation();
        Coord center = Coord(obj->getPosition().getX() + obj->getSize().getX() / 2.0,
                             obj->getPosition().getY() + obj->getSize().getY() / 2.0,
                             obj->getPosition().getZ() + obj->getSize().getZ() / 2.0);

        glTranslated(center.getX(), center.getY(), center.getZ());

        glRotated(rotate.getX(), 1.0, 0.0, 0.0);
        glRotated(rotate.getY(), 0.0, 1.0, 0.0);
        glRotated(rotate.getZ(), 0.0, 0.0, 1.0);

        glTranslated(-center.getX(), -center.getY(), -center.getZ());

        glTranslated(pos.getX(), pos.getY(), pos.getZ());

        obj->display();

        glPopMatrix();
    }
}

void Region::animate(double secsElapsed)
{
    for (shared_ptr<WorldObject> obj : getObjects())
        obj->animate(secsElapsed);
}

shared_ptr<WorldObject> Region::getObjectContaining(const Coord &c)
{
    if (c.getX() < getX() || c.getZ() < getZ() || c.getX() >= getX() + getWidth()
            || c.getZ() >= getZ() + getHeight())
        return shared_ptr<WorldObject>(NULL);

    for (shared_ptr<WorldObject> obj : getObjects())
    {
        if (obj->getBoundingBox().contains(c))
            return obj;
    }

    return shared_ptr<WorldObject>(NULL);
}
